package Modules;

/**
 * Created by Kenneth Misigaro on May 2018.
 */
public class Distance {
    public String text;
    public int value;

    public Distance(String text, int value) {
        this.text = text;
        this.value = value;
    }
}